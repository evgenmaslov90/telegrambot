package com.elitaliqid.elitaliqid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class  ElitaliqidApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElitaliqidApplication.class, args);
	}

}
