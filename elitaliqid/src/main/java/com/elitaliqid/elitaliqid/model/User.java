package com.elitaliqid.elitaliqid.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter

@AllArgsConstructor
@NoArgsConstructor
public class User {

    private Long chatId;

    private String name;

    private String dataOfRegistered;

}
