package com.elitaliqid.elitaliqid.service;

import com.elitaliqid.elitaliqid.config.BotConfig;
import com.elitaliqid.elitaliqid.model.User;
import com.vdurmont.emoji.EmojiParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class TelegramBot extends TelegramLongPollingBot {

    final BotConfig config;

    public TelegramBot(BotConfig config) {

        this.config = config;
        List<BotCommand> listofCommands = new ArrayList<>();
        listofCommands.add(new BotCommand("/start", " Welcome command"));
        listofCommands.add(new BotCommand("/admin", " Connect with admin"));
        listofCommands.add(new BotCommand("/cart", " Show my cart"));
        listofCommands.add(new BotCommand("/help", " Info how to use this bot"));
        listofCommands.add(new BotCommand("/buy", " Admin write u, or u can"));
        try {
            this.execute(new SetMyCommands(listofCommands, new BotCommandScopeDefault(), null));
        } catch (TelegramApiException e) {
            log.error("Error settings bots command list: " + e.getMessage());
        }

    }

    @Override
    public String getBotUsername() {
        return config.getBotName();
    }

    @Override
    public String getBotToken() {
        return config.getToken();
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText();
            long chatID = update.getMessage().getChatId();

            if(messageText.contains("/send")){
                var textToSend = EmojiParser.parseToUnicode(messageText.substring(messageText.indexOf(" ")));
                List<User> users = new ArrayList<User>(List.of(new User(1L, "Vito", "03-02-2023")));
                for (User user: users){
                    sendMessage(user.getChatId(), textToSend);
                }
            }
            switch (messageText) {
                case "/start":
                    startCommandReceived(chatID, update.getMessage().getChat().getFirstName());
                    break;
                case "/cart":
                    getCart(chatID);
                    break;
                case "/help":
                    try {
                        helpCommandReceived(chatID);
                    } catch (TelegramApiException e) {
                        log.error("Error with photo " + e.getMessage());
                    }
                    break;
                case "/send":
                    break;
                default:
                    sendMessage(chatID, "Sorry, command was not recognized");
            }
        }
        else if(update.hasCallbackQuery()){
            String callBackData = update.getCallbackQuery().getData();
            long messageId = update.getCallbackQuery().getMessage().getMessageId();
            long chatId = update.getCallbackQuery().getMessage().getChatId();

            if(callBackData.equals("Save_Button")) {
                String text = "Save data in cart";
                EditMessageText messageText = new EditMessageText();
                messageText.setChatId(String.valueOf(chatId));
                messageText.setText(text);
                messageText.setMessageId((int) messageId);
                try {
                    execute(messageText);
                } catch (TelegramApiException e) {
                    log.error("Error occurred: " + e.getMessage());
                }
            } else if(callBackData.equals("Delete_Button")){
                String text = "Delete data in cart";
                EditMessageText messageText = new EditMessageText();
                messageText.setChatId(String.valueOf(chatId));
                messageText.setText(text);
                messageText.setMessageId((int) messageId);
                try {
                    execute(messageText);
                } catch (TelegramApiException e) {
                    log.error("Error occurred: " + e.getMessage());
                }
            }

        }
    }

    private void getCart(Long chatId) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText("Its ur cart, If u wanna but it all click /buy ");

        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInLine = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        var saveButton = new InlineKeyboardButton();
        saveButton.setText("Save");
        saveButton.setCallbackData("Save_Button");

        var deleteButton = new InlineKeyboardButton();
        deleteButton.setText("Delete");
        deleteButton.setCallbackData("Delete_Button");

        rowInline.add(saveButton);
        rowInline.add(deleteButton);

        rowsInLine.add(rowInline);

        log.info("Buttons created");

        markup.setKeyboard(rowsInLine);
        message.setReplyMarkup(markup);

        try {
            execute(message);
        } catch (TelegramApiException e) {
            log.error("Error occurred: " + e.getMessage());
        }
    }

    private void helpCommandReceived(long chatID) throws TelegramApiException {

        sendMessage(chatID, "Nice to meet u in our shop bot");
        sendMessage(chatID, ":-)");
        /* execute(SendPhoto.builder()
                .chatId(String.valueOf(chatID))
                .photo(new InputFile(new File("photo/maxresdefault.jpg"))).build());
        */
    }

    private void startCommandReceived(long chatId, String name) {

        String answer = EmojiParser.parseToUnicode("Добро пожаловать," + name + ",в шоп лучших жижок" + ":blush:");
        log.info("Replied to user " + name);
        sendMessage(chatId, answer);
    }

    private void sendMessage(long chatId, String textToSend) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(chatId));
        sendMessage.setText(textToSend);
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();

        List<KeyboardRow> keyboardRowList = new ArrayList<>();

        KeyboardRow row = new KeyboardRow();
        row.add("Pods");
        row.add("Smoke Bar");
        keyboardRowList.add(row);

        row = new KeyboardRow();
        row.add("Cart");
        keyboardRowList.add(row);


        keyboardMarkup.setKeyboard(keyboardRowList);
        keyboardMarkup.setResizeKeyboard(true);
        sendMessage.setReplyMarkup(keyboardMarkup);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            log.error("Error occurred: " + e.getMessage());
        }
    }
}

